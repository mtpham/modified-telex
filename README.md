# modified-telex

As a Vietnamese and English speaker, I have a constant need to write in both English and Vietnamese concurrently. Among the existing Vietnamese typing engines, Telex appears to be very convenient as the typing is restricted to the 26 English letters, leading to high portability among devices like PCs, tablets and phones. However, I find myself constantly switching between Vietnamese and Engligh typing engines because typing in Telex can lead to a conflict with an English word with high probability. It would be great to have a new typing engine with little modification such that the probability of encountering a conflict with an English word is very low. Hence, the need to create this project.

To solve the problem, I have collected in this repository relevant materials so that future optimisation of the engine can be made.

Bottom line: to get from max 6.7% to max below 0.017% probability of getting a conflict, we change:
* aa, oo, ee -> az, oz, ez
* aw, dd -> aq, dq
* ow, uw -> ox, ux
* r -> jn (hoi)
* s -> jk (sac)
* f -> jh (huyen)
* x -> jm (nga)
* j -> jj (nang)
* z -> jl (clear)

Rename this to Envi: an English-Vietnamese typing scheme

2019/09/22
----------

According to the analysis done in 2018 in the data/ folder, we should try the following typing scheme whose conflict probability is super low:

    xz for sắc
    zx for huyền
    cx for nặng
    dx for hỏi
    xx for ngã
    eqq, oqq, aqq, dqq for ê, ô, â, đ
    ujj, ojj, ajj for ư, ơ, ă


2019/09/23
----------

After trying with VNI and Telex again, it still feels like tones are number one reason for conflicts. But at the same time, it is quite tiring to have to use 2 keys for a tone. And VNI's weakness is that one has to move the hands up to the numeric row too often, making it slow to type and quick to tire. It is still better to type in the 3 alphabet rows with as few strokes as possible.

Single key stroke for a tone with lowest probability of hitting an English bigram

  * Q (0.001030) for sắc
  * Z (0.001142) for huyền
  * X (0.001933) for nặng
  * J (0.002077) for hỏi
  * K (0.008084) for ngã

Single suffix key to form a Vietnamese letter with very low probability of hitting an English bigram

  * aa (0.000398) for â
  * aw (0.000906) for ă
  * ee (0.004278) for ê
  * oo (0.002352) for ô
  * op (0.002419) for ơ
  * uu (0.000015) for ư
  * dd (0.000925) for đ
